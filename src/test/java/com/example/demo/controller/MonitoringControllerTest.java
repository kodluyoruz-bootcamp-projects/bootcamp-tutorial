package com.example.demo.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringController.class)
public class MonitoringControllerTest {

    @Autowired
    private MonitoringController monitoringController;

    @Test
    public void it_should_return_live_ok() {
        // given

        // when
        final String live = monitoringController.live();

        // then
        assertEquals("OK", live);
    }

    @Test
    public void it_should_return_ready_ok() {
        // given

        // when
        final String ready = monitoringController.ready();

        // then
        assertEquals("OK", ready);
    }
}