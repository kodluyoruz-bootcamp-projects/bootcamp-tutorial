package com.example.demo.model.request;

import com.example.demo.domain.Product;

import javax.validation.constraints.NotNull;

import static com.example.demo.domain.Product.ProductBuilder.aProduct;

public class ProductCreateRequest {

    @NotNull
    private String name;

    @NotNull
    private Integer quantity;

    public Product toProduct() {
        return aProduct()
                .name(this.name)
                .quantity(this.quantity)
                .build();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public static final class ProductRequestBuilder {
        private String name;
        private Integer quantity;

        private ProductRequestBuilder() {
        }

        public static ProductRequestBuilder aProductRequest() {
            return new ProductRequestBuilder();
        }

        public ProductRequestBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductRequestBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public ProductCreateRequest build() {
            ProductCreateRequest productCreateRequest = new ProductCreateRequest();
            productCreateRequest.setName(name);
            productCreateRequest.setQuantity(quantity);
            return productCreateRequest;
        }
    }
}
