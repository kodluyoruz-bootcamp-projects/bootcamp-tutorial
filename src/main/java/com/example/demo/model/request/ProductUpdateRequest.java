package com.example.demo.model.request;

import com.example.demo.domain.Product;

import javax.validation.constraints.NotNull;

import static com.example.demo.domain.Product.ProductBuilder.aProduct;

public class ProductUpdateRequest {

    @NotNull
    private String name;

    @NotNull
    private Integer quantity;

    public Product toProduct(Long id) {
        return aProduct()
                .id(id)
                .name(this.name)
                .quantity(this.quantity)
                .build();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public static final class ProductUpdateRequestBuilder {
        private String name;
        private Integer quantity;

        private ProductUpdateRequestBuilder() {
        }

        public static ProductUpdateRequestBuilder aProductUpdateRequest() {
            return new ProductUpdateRequestBuilder();
        }

        public ProductUpdateRequestBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductUpdateRequestBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public ProductUpdateRequest build() {
            ProductUpdateRequest productUpdateRequest = new ProductUpdateRequest();
            productUpdateRequest.setName(name);
            productUpdateRequest.setQuantity(quantity);
            return productUpdateRequest;
        }
    }
}
