package com.example.demo.model.response;

public class ProductResponse {

    private Long id;
    private String name;
    private Integer quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public static final class ProductResponseBuilder {
        private Long id;
        private String name;
        private Integer quantity;

        private ProductResponseBuilder() {
        }

        public static ProductResponseBuilder aProductResponse() {
            return new ProductResponseBuilder();
        }

        public ProductResponseBuilder id(Long id) {
            this.id = id;
            return this;
        }

        public ProductResponseBuilder name(String name) {
            this.name = name;
            return this;
        }

        public ProductResponseBuilder quantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public ProductResponse build() {
            ProductResponse productResponse = new ProductResponse();
            productResponse.setId(id);
            productResponse.setName(name);
            productResponse.setQuantity(quantity);
            return productResponse;
        }
    }
}
