package com.example.demo.service;

import com.example.demo.domain.Product;
import com.example.demo.model.request.ProductCreateRequest;
import com.example.demo.model.request.ProductUpdateRequest;
import com.example.demo.model.response.ProductResponse;
import com.example.demo.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<ProductResponse> getProducts() {
        return productRepository.findAll().stream()
                .map(Product::toProductResponse)
                .collect(Collectors.toList());
    }

    public ProductResponse getProduct(Long id) throws Exception {
        return productRepository.findById(id)
                .map(Product::toProductResponse)
                .orElseThrow(() -> new Exception("product.not.found"));
    }

    public ProductResponse saveProduct(ProductCreateRequest request) {
        return productRepository.save(request.toProduct()).toProductResponse();
    }

    public ProductResponse updateProduct(Long id, ProductUpdateRequest request) {
        return productRepository.save(request.toProduct(id)).toProductResponse();
    }

    public void deleteProduct(Long id) {
        productRepository.deleteById(id);
    }
}
