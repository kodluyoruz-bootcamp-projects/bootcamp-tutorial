#FROM maven:3-jdk-8-alpine AS build
#WORKDIR /tmp
#COPY . .
#ARG VERSION
#RUN mvn verify --no-transfer-progress -Dapp.version=$VERSION -DskipTests=true

FROM adoptopenjdk/openjdk8-openj9:jre8u222-b10_openj9-0.15.1-alpine
COPY app.jar .
EXPOSE 8080
ENV LANG en_GB.UTF-8
RUN apk update && apk add bash && apk add --update ttf-dejavu && rm -rf /var/cache/apk/* && apk add fontconfig
ENTRYPOINT java -jar /app.jar
